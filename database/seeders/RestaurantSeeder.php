<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $restaurants = [
            [
                'name' => 'Perle',
                'distance' => '*',
                'price' => '*',
                'veggie' => '***',
                'address' => 'Spitalerstraße 22, 20095 Hamburg',
                'category' => 'all',
            ],
            [
                'name' => 'Europapassage',
                'distance' => '*',
                'price' => '**',
                'veggie' => '***',
                'address' => 'Ballindamm 40 EG2, 20095 Hamburg',
                'category' => 'all',
            ],
            [
                'name' => 'Max & Consorten',
                'distance' => '***',
                'price' => '*',
                'veggie' => '**',
                'address' => 'Spadenteich 1, 20099 Hamburg',
                'category' => 'homemade',
            ],
            [
                'name' => "Luigi's",
                'distance' => '***',
                'price' => '**',
                'veggie' => '***',
                'address' => 'Ditmar-Koel-Straße 21, 20459 Hamburg',
                'category' => 'pizza_pasta',
            ],
            [
                'name' => 'Bella Italia',
                'distance' => '**',
                'price' => '*',
                'veggie' => '**',
                'address' => 'Brandstwiete 58, 20457 Hamburg',
                'category' => 'pizza_pasta',
            ],
            [
                'name' => 'Restaurant Kabul',
                'distance' => '***',
                'price' => '*',
                'veggie' => '**',
                'address' => 'Steindamm 53, 20099 Hamburg',
                'category' => 'others',
            ],
            [
                'name' => 'Goot',
                'distance' => '**',
                'price' => '***',
                'veggie' => '*',
                'address' => 'Depenau 10, 20095 Hamburg',
                'category' => 'homemade',
            ],
            [
                'name' => 'O-ren Ishii',
                'distance' => '**',
                'price' => '***',
                'veggie' => '***',
                'address' => 'Kleine Reichenstraße 18, 20457 Hamburg',
                'category' => 'asian',
            ],
            [
                'name' => 'Better Burger Company',
                'distance' => '*',
                'price' => '**',
                'veggie' => '***',
                'address' => 'Rosenstraße Ecke, Gertrudenkirchhof, 20095 Hamburg',
                'category' => 'burger',
            ],
            [
                'name' => 'Bucks Burgers',
                'distance' => '**',
                'price' => '**',
                'veggie' => '***',
                'address' => 'Kurze Mühren 13, 20095 Hamburg',
                'category' => 'burger',
            ],
            [
                'name' => 'Mr. Cherng',
                'distance' => '**',
                'price' => '***',
                'veggie' => '***',
                'address' => 'Speersort 1, 20095 Hamburg',
                'category' => 'asian',
            ],
            [
                'name' => 'Franco Rathauspassage',
                'distance' => '**',
                'price' => '**',
                'veggie' => '***',
                'address' => 'Rathausmarkt 7, 20095 Hamburg',
                'category' => 'pizza_pasta',
            ],


        ];



        foreach ($restaurants as $restaurant) {
            DB::table('restaurants')->insert([
                'name' => $restaurant['name'],
                'distance' => $restaurant['distance'],
                'price' => $restaurant['price'],
                'veggie' => $restaurant['veggie'],
                'address' => $restaurant['address'],
                'category' => $restaurant['category'],
            ]);
        }
    }
}
