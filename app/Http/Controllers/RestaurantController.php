<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{

    public function getResults(Request $request)
    {
        $params = $request->all();

        $results = Restaurant::query();



        foreach ($params as $key => $value) {
            if ($key == 'category') {
                if ($value[0] != 'all') {
                    array_push($value, 'all');
                    $results->whereIn($key, $value);
                }
            } else {
                if ($value != 'all') {
                    $results->where($key, $value);
                }
            }
        }

        $results = $results->orderBy('id')->get();

        return ['restaurants' => $results];
    }
}
