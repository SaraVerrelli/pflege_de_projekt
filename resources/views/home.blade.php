<x-layout>
    <div class="container-fluid p-sm-4">
        <div class="row mx-auto">
            <h1>WO / WAS ESSEN WIR DENN HEUTE?</h1>
        </div>
        <div id="filters_div">
            <div class="row mx-auto">
                <h4 class="mt-3">Kategorien - FILTER</h4>
                <div class="btn-group mt-2" role="group" aria-label="Categories checkbox toggle button group">
                    <input type="checkbox" class="filter_input btn-check" name="categoriesCheckbox" id="all" value="all"
                        autocomplete="off" checked>
                    <label class="btn btn-outline-dark" for="all">Alles</label>

                    <input type="checkbox" class="filter_input cat_input btn-check" name="categoriesCheckbox" id="burger"
                        value="burger" autocomplete="off">
                    <label class="btn btn-outline-dark" for="burger">Burger</label>

                    <input type="checkbox" class="filter_input cat_input btn-check" name="categoriesCheckbox"
                        id="pizza_pasta" value="pizza_pasta" autocomplete="off">
                    <label class="btn btn-outline-dark" for="pizza_pasta">Pizza/Pasta</label>

                    <input type="checkbox" class="filter_input cat_input btn-check" name="categoriesCheckbox" id="asian"
                        value="asian" autocomplete="off">
                    <label class="btn btn-outline-dark" for="asian">Asiatisch</label>

                    <input type="checkbox" class="filter_input cat_input btn-check" name="categoriesCheckbox"
                        id="homemade" value="homemade" autocomplete="off">
                    <label class="btn btn-outline-dark" for="homemade">Hausmannskost</label>

                    <input type="checkbox" class="filter_input cat_input btn-check" name="categoriesCheckbox"
                        id="others" value="others" autocomplete="off">
                    <label class="btn btn-outline-dark" for="others">Sonstiges</label>

                </div>

            </div>

            <div class="row mx-auto">
                <div class="col-12 col-md-6 mt-3">
                    <h4>Entfernung</h4>
                    <div class="btn-group mt-2" role="group" aria-label="Distance radio toggle button group">
                        <input type="radio" class="filter_input btn-check" name="distanceRadio" id="no_matter_distance"
                            value="all" autocomplete="off" checked>
                        <label class="btn btn-outline-dark" for="no_matter_distance">Egal</label>

                        <input type="radio" class="filter_input btn-check" name="distanceRadio" id="not_too_far"
                            value="**" autocomplete="off">
                        <label class="btn btn-outline-dark" for="not_too_far">Nicht so weit weg</label>

                        <input type="radio" class="filter_input btn-check" name="distanceRadio" id="close" value="*"
                            autocomplete="off">
                        <label class="btn btn-outline-dark" for="close">Ganz nah dran</label>

                    </div>
                </div>
                <div class="col-12 col-md-6 mt-3">
                    <h4>Preis</h4>
                    <div class="btn-group mt-2" role="group" aria-label="Price radio toggle button group">
                        <input type="radio" class="filter_input btn-check" name="priceRadio" id="no_matter_price"
                            value="all" autocomplete="off" checked>
                        <label class="btn btn-outline-dark" for="no_matter_price">Egal</label>

                        <input type="radio" class="filter_input btn-check" name="priceRadio" id="not_too_much"
                            value="**" autocomplete="off">
                        <label class="btn btn-outline-dark" for="not_too_much">Nicht zu viel</label>

                        <input type="radio" class="filter_input btn-check" name="priceRadio" id="cheap" value="*"
                            autocomplete="off">
                        <label class="btn btn-outline-dark" for="cheap">Ende des Monats</label>

                    </div>
                </div>


            </div>

            <div class="row mx-auto mt-3">
                <h4>Veggietauglich</h4>
                <div class="btn-group mt-2" role="group" aria-label="Vegetarian radio toggle button group">
                    <input type="radio" class="filter_input btn-check" name="vegetarianRadio" id="no_matter_veggie"
                        value="all" autocomplete="off" checked>
                    <label class="btn btn-outline-dark" for="no_matter_veggie">Egal</label>

                    <input type="radio" class="filter_input btn-check" name="vegetarianRadio" id="some" value="**"
                        autocomplete="off">
                    <label class="btn btn-outline-dark" for="some">Sollte schon schmecken</label>

                    <input type="radio" class="filter_input btn-check" name="vegetarianRadio" id="tasty" value="***"
                        autocomplete="off">
                    <label class="btn btn-outline-dark" for="tasty">Muss ganz lecker sein</label>

                </div>

            </div>

        </div>

        <div class="row mx-auto mt-3">
            <div class="col-12 col-sm-6">
                <h3>Ergebnisse</h3>
                <div class="row mx-auto mt-2">
                    <div id="results_div" class="border border-3 w-auto border-dark p-4">
                        <p class="text-muted">Wähle deine Filtern und start die Suche</p>
                    </div>

                </div>

            </div>

            <div class="col-12 col-sm-6 position-relative">
                <div class="position-absolute bottom-0 end-0">
                    <div id="randomize_btn" class="btn btn-dark float-end">Randomize</div>

                    <div class="btn btn-outline-dark float-end me-3" id="reset_btn">Reset</div>
                </div>

            </div>
        </div>


    </div>
</x-layout>
