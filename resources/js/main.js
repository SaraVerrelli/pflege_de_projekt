$(function () {
    $("#randomize_btn").on("click", function () {
        let parameters = getParameters();

        $.ajax({
            method: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/get-results",
            data: {
                category: parameters["category"],
                distance: parameters["distance"],
                price: parameters["price"],
                veggie: parameters["veggie"],
            },
            success: function (data) {
                let results_div = $("#results_div");
                let restaurants = data.restaurants;
                let list = 1;
                results_div.html("");
                restaurants.forEach((restaurant) => {
                    results_div.append(
                        `<div><p>${list}. ${restaurant["name"]} | <i class="fas fa-coins text-warning"></i> ${restaurant["price"]} | <i class="fas fa-car-side text-danger"></i> ${restaurant["distance"]} | <i class="fas fa-leaf text-success"></i> ${restaurant["veggie"]}</p></div>`
                    );

                    list += 1;
                });

                if (restaurants.length == 0) {
                    results_div.html(
                        `<div><p><i class="fa fa-solid fa-bone"></i> Keine Einträge gefunden <i class="fa fa-solid fa-bone"></i> </p></div>`
                    );
                }
            },
            error: function () {},
        });
    });

    $("#reset_btn").on("click", function () {
        $(".filter_input").prop("checked", false);
        $(".filter_input[value='all']").prop("checked", true);
    });

    $("#all").on("click", function () {
        $("input[name='categoriesCheckbox']").prop("checked", false);
        $(this).prop("checked", true);
    });

    $(".cat_input").on("click", function () {
        if (this.checked) {
            if ($(".cat_input").length == $(".cat_input:checked").length) {
                $(".cat_input").prop("checked", false);
                $("#all").prop("checked", true);
            } else {
                $("#all").prop("checked", false);
            }
        } else {
            if ($(".cat_input:checked").length == 0) {
                $("#all").prop("checked", true);
            }
        }
    });

    function getParameters() {
        let category = [];
        let distance = $("input[name='distanceRadio']:checked").val();
        let price = $("input[name='priceRadio']:checked").val();
        let veggie = $("input[name='vegetarianRadio']:checked").val();
        $("input[name='categoriesCheckbox']:checked").map(function () {
            category.push($(this).val());
        });

        let parameters = [];

        parameters["category"] = category;
        parameters["distance"] = distance;
        parameters["price"] = price;
        parameters["veggie"] = veggie;

        return parameters;
    }
});
